# WPFCap

## WebCam control for WPF

Reusable WPF control to display high frame rate video such as WebCam or framegrabber DirectShow output. This control uses InteropBitmap introduced in .NET framework 3.5 and managed DirectShow P/Invoke. This control does not uses DirectShow.net library, so it's completely independent

## Usage
You can use the Nuget Package to get the control in your project:
[NuGet](https://www.nuget.org/packages/WPFCap/)

The usage of the control is similar to the usage of regular WPF Image control

```
	<cap:CapPlayer xmlns:cap="http://schemas.sharpsoft.net/xaml"/>
```

To set the camera used by the control see this example:

```
	var camMonikers = CapDevice.DeviceMonikers;
    foreach(cm in camMonikers)
    {
        if (cm.MonikerString.Contains("pid_075d") && cm.MonikerString.Contains("vid_045e"))
        {
            this.player.Device = new CapDevice(cm.MonikerString);
            break;
        }
        if (cm.MonikerString.Contains("pid_0812") && cm.MonikerString.Contains("vid_045e"))
        {
            this.player.Device = new CapDevice(cm.MonikerString);
            break;
        }
        if (cm.MonikerString.Contains("pid_0805") && cm.MonikerString.Contains("vid_046d"))
        {
            this.player.Device = new CapDevice(cm.MonikerString);
            break;
        }
    }
```

## Origins
This repository contains the data from a archived CodePlex repository: 
[Archived CodePlex Respository](https://archive.codeplex.com/?p=wpfcap)

